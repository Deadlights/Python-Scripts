

'''
The task is broken down into three sections.
Section 1 - User Input
Section 2 - loop through the grocery list
Section 3 - provide output to the console
'''

#Example of creating a dictionary
grocery_item = {}

#Example of creating a list
grocery_history = []

#Variable for condition of while loop
stop = 'go'
#Example of a while loop
while stop != 'q':

    #Request user input for item name
    item_name = input("Item name:\n")
    #Request user input for item quantity
    quantity = input("Quantity purchased:\n")
    #Requst user input for for item cost
    cost = input("Price per item:\n")
    #Example of adding key-value pairs to a dictionary item using gathered input
    grocery_item = {'name':item_name, 'number': int(quantity), 'price': float(cost)}
    #Example of adding an item to a list using the append function
    grocery_history.append(grocery_item)
    #Request user input to determine state of while loop - a 'q' will end loop
    stop = input("Would you like to enter another item?\nType 'c' for continue or 'q' to quit:\n")

#Given variable will hold the grand total
grand_total = 0

#Example of an item-based for loop - Example of accessing values in a list
for grocery_item in grocery_history:
  
  #Variable for total cost of grocery item - Example of accessing values with keys
  item_total = grocery_item['number'] * grocery_item['price']
  #Accumulator for grand total
  grand_total += item_total
  #Output of information for grocery item  - in the output there is an example of accessing values using keys
  print("{0} {1} @ ${2} ea ${3}".format(grocery_item['number'], grocery_item['name'], '{:,.2f}'.format(grocery_item['price']), '{:,.2f}'.format(item_total)))
  
  
  #Set item total to equal 0
  item_total = 0
#Print the grand total
print("Grand total: ${:,.2f}".format(grand_total))

#Example - There are no index-based for loops in this assignment 
#Example - There are no list values being modified in this assignment
#Example - There are no dictionary values being modified in this assignment
